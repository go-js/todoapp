import React from 'react';
import './TaskList.css';

import Task from './Task'

const TaskList = (props) => {

    const activeTasks = props.tasks.filter(task => task.active)
        .sort((task1, task2) => new Date(task1.date) - new Date(task2.date))
        .map(task => (
            <Task
                key={task.id}
                task={task}
                delete={props.delete}
                change={props.change}
            />
        ))
    const doneTasks = props.tasks.filter(task => task.active === false)
        .sort((task1, task2) => task2.finishDate - task1.finishDate)
        .map(task => (
            <Task
                key={task.id}
                task={task}
                delete={props.delete}
                change={props.change}
                restore={props.restore}

            />
        ))


    return (
        <>
            <div className="active">
                <h2>To Do Tasks</h2>
                <table id="todo">
                    <tbody>
                        <tr>
                            <th>Task</th>
                            <th>Deadline</th>
                            <th>Action</th>
                        </tr>
                        {activeTasks.length > 0 ? activeTasks : <p>nothing to do</p>}
                    </tbody>
                </table>
            </div>
            <hr />
            {doneTasks.length > 0 &&
                <div className="done">
                    <h2>Done Tasks</h2>
                    {doneTasks.length > 5 && <span>Wyświetlonych jest jedynie ostatnich 5 zadań</span>}
                    <table id="todo">
                        <tbody>
                            <tr>
                                <th>Task</th>
                                <th>Deadline date</th>
                                <th>Finish Date</th>
                                <th>Actions</th>
                            </tr>
                            {doneTasks.slice()}
                        </tbody>
                    </table>
                </div>
            }
        </>
    );
}

export default TaskList;