import React, { Component } from 'react';
import './App.css';

import AddTask from './AddTask'
import TaskList from './TaskList'


class App extends Component {

  state = {
    tasks: [
      {
        id: 0,
        text: 'zagrać w grę',
        date: '2019-12-01',
        important: true,
        active: true,
        finishDate: null
      },
      {
        id: 1,
        text: 'posprzątać na biurku',
        date: '2019-11-10',
        important: true,
        active: true,
        finishDate: null
      },
      {
        id: 2,
        text: 'Wyjść na spotkanie',
        date: '2018-01-05',
        important: true,
        active: true,
        finishDate: null
      },
      {
        id: 3,
        text: 'Napisać aplikację',
        date: '2020-01-15',
        important: true,
        active: true,
        finishDate: null
      },
      {
        id: 4,
        text: 'Pouczyć się',
        date: '2020-02-01',
        important: true,
        active: true,
        finishDate: null
      },
      {
        id: 5,
        text: 'Pouczyć się reacta',
        date: '2020-02-21',
        important: true,
        active: true,
        finishDate: null
      },
      {
        id: 6,
        text: 'Przeczytać książkę',
        date: '2020-01-15',
        important: true,
        active: true,
        finishDate: null
      },
    ]
  }

  deleteTask = (id) => {
    const tasks = Array.from(this.state.tasks);
    const index = tasks.findIndex(task => task.id === id);
    tasks.splice(index, 1);
    this.setState({
      tasks
    })
  }

  changeTaskStatus = (id) => {
    let tasks = [...this.state.tasks]
    tasks = tasks.map(task => {
      if (task.id === id) {
        task.active = false;
        task.finishDate = new Date();
      }
      return task
    })
    this.setState({
      tasks,
    })
  }

  handleRestore = (id) => {
    let tasks = [...this.state.tasks]
    tasks = tasks.map(task => {
      if (task.id === id) {
        task.active = true;
        task.finishDate = null;
      }
      return task
    })
    this.setState({
      tasks,
    })
  }


  handleAdd = (text, date, important) => {
    let tasks = [...this.state.tasks]
    let id = 0;
    if (tasks.length > 0) {
      id = tasks[tasks.length - 1].id + 1;
    }
    const task = {
      id: id,
      text,
      date,
      important,
      active: true,
      finishDate: null
    }
    this.setState({
      tasks: [...tasks, task]
    })
    return true;
  }


  render() {
    return (
      <div className="App">
        <h1>To do app</h1>
        <div className="content">
          <AddTask add={this.handleAdd} />
          <TaskList tasks={this.state.tasks} restore={this.handleRestore} delete={this.deleteTask} change={this.changeTaskStatus} />
        </div>
      </div>
    );
  }
}

export default App;
