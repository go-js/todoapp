import React from 'react';

const Task = (props) => {

    const alertStyle = {
        color: 'red',
    }
    const successStyle = {
        color: 'green',
    }

    const { id, text, date: deadLine, important, active, finishDate } = props.task
    if (active) {
        return (
            <>
                <tr>
                    <th style={important ? alertStyle : null}>{text}</th>
                    <th>{deadLine}</th>
                    <th>
                        <button onClick={() => props.delete(id)}> Delete </button>
                        <button onClick={() => props.change(id)}> Done </button>
                    </th>
                </tr>
            </>
        );
    } else {
        const dayTime = 1000 * 60 * 60 * 24;
        const finishDateNormalize = new Date(new Date(finishDate).toISOString().slice(0, 10));
        const finish = new Date(finishDateNormalize).toLocaleDateString();
        const timeDifference = Math.round((new Date(finishDateNormalize) - new Date(deadLine)) / dayTime);
        const deadlineExceeded = timeDifference > 0;
        const timeDffAbs = Math.abs(timeDifference);
        const info = <em style={deadlineExceeded ? alertStyle : successStyle}>
            Done {deadlineExceeded ? <>{timeDffAbs} day(s) after deadline</> : <>{timeDffAbs} day(s) before deadline</>}</em>
        return (
            <>
                <tr>
                    <th>{text}</th>
                    <th>{deadLine}</th>
                    <th>{finish} <br />{info}</th>
                    <th><button onClick={() => props.delete(id)}> Delete </button>
                        <button onClick={() => props.restore(id)}> Restore </button></th>
                </tr>
            </>
        )
    }
}

export default Task;