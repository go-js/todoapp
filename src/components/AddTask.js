import React, { Component } from 'react';
import './AddTask.css'
import { timeout } from 'q';

class AddTask extends Component {
    minDate = new Date().toISOString().slice(0, 10);
    state = {
        text: '',
        checked: false,
        date: this.minDate,
        message: "",
    }


    handleChange = (e) => {
        const type = e.target.type;
        if (type === 'text') {
            this.setState({
                text: e.target.value
            })
        }
        if (type === 'checkbox') {
            this.setState(prev => ({
                checked: !prev.checked
            }))
        }
        if (type === 'date') {
            this.setState({
                date: e.target.value
            })
        }
    }

    handleClick() {
        const { text, date, checked } = this.state
        let isAdded = false
        let isValid = false;
        if (text.length > 3 && date !== null) {
            isValid = true;
        } else {
            this.setState({
                message: "form filled incorrectly"
            })
        }
        if (isValid) {
            isAdded = this.props.add(text, date, checked)
        }
        if (isAdded) {
            this.setState({
                text: '',
                checked: false,
                date: this.minDate,
                message: "task added to list"
            })
        }
    }

    componentDidUpdate() {
        if (this.state.message != '') {
            setTimeout(() => {
                this.setState({
                    message: ''
                })
            }, 1000);
        }
    }

    render() {
        const today = this.minDate
        const maxDate = this.minDate.slice(0, 2) * 1 + 1 + "12-31";
        const { text, date, checked } = this.state
        return (
            <div className="todo-app-form">
                <div className="form">
                    <div className="field1">
                        <input
                            type="text"
                            placeholder="add new task"
                            onChange={this.handleChange}
                            value={this.state.text} />
                        <input
                            type="checkbox"
                            id='important'
                            onChange={this.handleChange}
                            checked={this.state.checked} />
                        <label htmlFor='important'>priority</label>

                    </div>
                    <div className="field2">
                        <label htmlFor="date">Deadline:</label>
                        <input type="date" value={this.state.date} min={this.minDate} max="2020-12-31" onChange={this.handleChange} />
                    </div>
                    <div className="field3">
                    </div>
                    <button onClick={this.handleClick.bind(this)}>Add new task</button>
                    <div><em>{this.state.message}</em></div>
                </div>
            </div>
        );
    }
}

export default AddTask;